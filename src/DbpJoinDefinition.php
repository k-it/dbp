<?php

namespace kit;

class DbpJoinDefinition{

    private $masterTableAlias;
    private $masterTableIdColumn;
    private $slaveTable;
    private $slaveTableAlias;
    private $slaveTableIdColumn;
    private $fieldMapping;
    private $joinType = 'LEFT';

    function addField($slaveColumnName, $alias){
        $this->fieldMapping[] = [$slaveColumnName, $alias];
    }

    function updateMaster($tableAlias, $tableIdColumn): void{
        $this->setMasterTableAlias($tableAlias);
        $this->setMasterTableIdColumn($tableIdColumn);
    }

    function setJoinType($joinType){
        if(!preg_match('/^(LEFT|RIGHT|INNER)$/i', $joinType))
            throw new \InvalidArgumentException("Invalid jointype, use left, right or inner");
        $this->joinType = $joinType;
    }

    public static function fromArgs($slaveTable, $slaveTableAlias, $slaveTableIdColumn, $masterTableIdColumn): DbpJoinDefinition{
        $def = new DbpJoinDefinition();
        $def->setSlaveTable($slaveTable);
        $def->setSlaveTableAlias($slaveTableAlias);
        $def->setSlaveTableIdColumn($slaveTableIdColumn);
        $def->setMasterTableIdColumn($masterTableIdColumn);
        return $def;
    }

    function getMasterTableAlias(){
        return $this->masterTableAlias;
    }

    function getMasterTableIdColumn(){
        return $this->masterTableIdColumn;
    }

    function getSlaveTable(){
        return $this->slaveTable;
    }

    function getSlaveTableAlias(){
        return $this->slaveTableAlias;
    }

    function getSlaveTableIdColumn(){
        return $this->slaveTableIdColumn;
    }

    function getFieldMapping(){
        return $this->fieldMapping;
    }

    function getJoinType(){
        return $this->joinType;
    }

    function setMasterTableAlias($masterTableAlias){
        $this->masterTableAlias = $masterTableAlias;
    }

    function setMasterTableIdColumn($masterTableIdColumn){
        $this->masterTableIdColumn = $masterTableIdColumn;
    }

    function setSlaveTable($slaveTable){
        $this->slaveTable = $slaveTable;
    }

    function setSlaveTableAlias($slaveTableAlias){
        $this->slaveTableAlias = $slaveTableAlias;
    }

    function setSlaveTableIdColumn($slaveTableIdColumn){
        $this->slaveTableIdColumn = $slaveTableIdColumn;
    }

    function setFieldMapping($fieldMapping){
        $this->fieldMapping = $fieldMapping;
    }

}
