<?php

namespace kit;

class DbpRepository{

    /**
     * @var \kit\dbp
     */
    protected $db;
    protected $cols;
    protected $table;
    protected $joins = [];
    public $LIMIT = 1000;

    function __construct(\kit\dbp $db){
        $this->db = $db;
    }

    function init($targetTable){
        if(empty($targetTable))
            throw new \InvalidArgumentException("Invalid targettable");
        $this->table = $targetTable;

        try{
            $this->cols = array_map(function ($row){
                return [
                'field' => $row['Field'],
                'type' => $row['Type'],
                'null' => ($row['Null'] == "YES" ? true : false)
                ];
            }, $this->db->getResult("SHOW COLUMNS IN {$this->table}"));
        }
        catch(\Exception $ex){
            // could not fetch columns, could be a different driver
        }
    }

    /**
     * Join extra table for getId and getAll methods
     */
    function joinTable(DbpJoinDefinition $definition){
        $this->joins[] = $definition;
    }

    /**
     * Create class-file for database table
     * @param string $namespace
     * @return bool
     * @throws DbpException
     */
    function createDbObject($namespace = "", $exportdir = null): bool{
        $sClassName = "Db" . ucfirst($this->table);
        $sFilename = (!empty($exportdir) ? $exportdir : dirname(__FILE__)) . "/{$sClassName}.php";
        if(file_exists($sFilename))
            throw new DbpException("File for {$sClassName} already exists");

        $s = "<?php\n\nnamespace {$namespace};\n\nclass {$sClassName}{\n\n";
        foreach($this->cols as $col){
            $s .= "\tpublic \${$col['field']};\n";
        }
        $s .= "\n\n\tpublic function toArray(){\n\t\treturn get_object_vars(\$this);\n\t}\n\t\n}";
        return file_put_contents($sFilename, $s);
    }

    function getNumRows(): int{
        return (int)$this->db->getValue("SELECT COUNT(*) FROM {$this->table}");
    }

    function getId($id){
        $sSql = $this->getSqlBaseSelect($this->joins) . " WHERE t.id = ?";
        $res = $this->db->getRow($sSql, [$id]);
        return empty($res) ? null : $res;
    }

    /**
     * Return all records, optionally ordered by [column,direction]
     * @param array $order
     * @return array
     */
    function getAll($order = null): ?array{
        return $this->db->getResult($this->getSqlBaseSelect($this->joins) . " {$this->createOrderBy($order)} LIMIT {$this->LIMIT} ");
    }

    function insertArray(array $param): int{
        $this->validateArray(array_keys($param));
        $a = $this->db->insertArray($this->table, $param);
        return (int) $a['insert_id'];
    }

    function updateArray(array $param, $id): bool{
        if(empty($id))
            throw new \InvalidArgumentException("Invalid id");
        $this->validateArray(array_keys($param));
        $a = $this->db->updateArray($this->table, $param, 'id', $id);
        return true;
    }

    function delete($id): bool{
        if(empty($id))
            throw new \InvalidArgumentException("Invalid id");
        $this->db->query("DELETE FROM {$this->table} WHERE id=? ", [$id]);
        return true;
    }

    /**
     * Search for [col,operator,value], order by [col, direction] with limit
     * 
     * @param ?array $param
     * @param ?array $order
     * @param int $limit
     * @return array|null
     */
    function search($param, $order = null, $limit = null): ?array{
        $limit = $limit ?? $this->LIMIT;
        $param = !$param ? [] : (!is_array($param[0]) ? [$param] : $param);
        $this->validateArray(array_map(function ($elm){
                return $elm[0] ?? null;
            }, $param));

        $where = "";
        $params = [];
        if(!empty($param)){
            array_walk($param, function ($elm) use (&$where, &$params){
                if(!in_array($elm[1] ?? null, ['=', '!=', 'LIKE', '>', "<"]))
                    throw new DbpException("Invalid operator");
                $params[] = $elm[2];
                $where .= empty($where) ? "" : (!empty($elm[3]) ? " {$elm[3]} " : " AND ");
                $where .= "\n t.`{$elm[0]}`{$elm[1]} ?  ";
            });
        }
        else
            $where .= "1=1";
        $sql = $this->getSqlBaseSelect($this->joins) . " WHERE {$where} {$this->createOrderBy($order)} LIMIT {$limit} ";
        return $this->db->getResult($sql, $params);
    }

    public function createOrderBy($param): string{
        if(empty($param) || !is_array($param))
            return "";
        if(!isset($param[0]))
            throw new \InvalidArgumentException("Invalid format");
        $param = !is_array($param[0]) ? [$param] : $param;
        $this->validateArray(array_map(function ($elm){
                return $elm[0] ?? null;
            }, $param));
        array_walk($param, function ($elm){
            if(!in_array(strtoupper($elm[1]), ['ASC', 'DESC', 'RAND()']))
                throw new \InvalidArgumentException("Invalid order: {$elm[1]}");
        });

        return " ORDER BY " . implode(",", array_map(function ($elm){
                    if($elm[1] == "RAND()")
                        return $elm[1];
                    return " t.`{$elm[0]}` {$elm[1]} ";
                }, $param));
    }

    protected function validateArray($arr){
        if($this->cols === null)
            return;
        array_walk($arr, function ($elm){
            foreach($this->cols as $col){
                if($col['field'] == $elm)
                    return;
            }
            throw new DbpException("Column not allowed: {$elm}");
        });
    }

    protected function getSqlBaseSelect($joins): string{
        $return = "SELECT t.* FROM {$this->table} t";
        if(empty($joins))
            return $return;

        $sqlTables = "";
        $sqlCols = "t.*";
        $sqlTables = "{$this->table} t";
        foreach($joins as $join){
            /* @var $join DbpJoinDefinition */
            if($join->getFieldMapping() == null)
                continue;
            $tableAlias = $join->getSlaveTableAlias();
            $sqlCols .= ", " . implode(",", array_map(function ($rec) use ($tableAlias){
                        return "{$tableAlias}.`{$rec[0]}` as '{$rec[1]}'";
                    }, $join->getFieldMapping()));

            $masterAlias = $join->getMasterTableAlias() ?? "t";
            $sqlTables .= "\n {$join->getJoinType()} JOIN {$join->getSlaveTable()} as {$join->getSlaveTableAlias()} ON {$masterAlias}.`{$join->getMasterTableIdColumn()}` = {$join->getSlaveTableAlias()}.`{$join->getSlaveTableIdColumn()}`";
        }
        return "SELECT {$sqlCols} FROM {$sqlTables} ";
    }

}
