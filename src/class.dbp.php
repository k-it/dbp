<?php

namespace kit;

class dbp{

    public $pdo;
    protected $connected = false;
    private static $instance;
    public $emptyReturnValue = null;

    /**
     * Return existing or create a new instance
     *
     * @return dbp  
     */
    public static function getInstance(){
        if(!self::$instance){ // If no instance then make one
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Set custom PDO connection to instance
     * @param \PDO $PDO
     * @return boolean
     */
    public function setPdoConnection($PDO = null){
        if($PDO == null)
            return false;
        $this->pdo = $PDO;
        return true;
    }

    /**
     * Connect to a PDO database
     *
     * @param   string  $p_sDbHost  
     * @param   string  $p_sDbUser  
     * @param   string  $p_sDbPass  
     * @param   string  $p_sDbName  
     *
     * @return  bool   
     */
    function connect($p_sDbHost = '', $p_sDbUser = '', $p_sDbPass = '', $p_sDbName = ''){

        $dsn = "mysql:host={$p_sDbHost};dbname={$p_sDbName};charset=utf8mb4";
        $options = [
            \PDO::ATTR_EMULATE_PREPARES => false, // turn off emulation mode for "real" prepared statements
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION, //turn on errors in the form of exceptions
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC, //make the default fetch be an associative array
            \PDO::ATTR_STRINGIFY_FETCHES => false // dont convert decimals/integers to string
        ];

        $this->pdo = new \PDO($dsn, $p_sDbUser, $p_sDbPass, $options);
        $this->connected = true;
        return true;
    }

    /**
     * Connect to a mysql database, using data in json-configfile
     * @param string $p_sConfig full path to config-file
     * @return bool on success
     */
    public function loadConfig($p_sConfig = null){

        # check if exists
        if(!file_exists($p_sConfig)){
            throw new DbpException("DB config not found");
        }

        # get data and connect
        $sSrc = @file_get_contents($p_sConfig);
        $a = json_decode($sSrc, true);
        if(isset($a['dbhost']) || isset($a['dbuser']) || isset($a['dbpass']) || isset($a['dbdatabase'])){
            return $this->connect($a['dbhost'], $a['dbuser'], $a['dbpass'], $a['dbdatabase']);
        }
        if(isset($a['host']) || isset($a['user']) || isset($a['pass']) || isset($a['database'])){
            return $this->connect($a['host'], $a['user'], $a['pass'], $a['database']);
        }

        throw new DbpException("Invalid DB config");
    }

    /**
     * Perform a query on the database
     * @param $p_sSql
     * @return array(affected_rows / insert_id)
     */
    function query($p_sSql, $params = null){

        $stmt = $this->pdo->prepare($p_sSql);
        $bRes = $stmt->execute($params);
        $aRes = array('success' => $bRes,
            'affected_rows' => $stmt->rowCount(),
            'insert_id' => $this->pdo->lastInsertId());

        return $aRes;
    }

    /**
     * Execute a query and return a multi-rowresult in an array.
     * @param $p_sSql
     * @return array
     */
    function getResult($p_sSql, $params = null){
        $stmt = $this->pdo->prepare($p_sSql);
        $stmt->execute($params);
        $arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        if(!$arr)
            return $this->emptyReturnValue;
        $stmt = null;
        return $arr;
    }

    /**
     * Execute a query and return a array with objects of given type.
     * @param $p_sSql
     * @param $p_sObject return-objects
     * @return array
     */
    function getObjectList($p_sSql, $p_sObject = "stdClass"){
        $stmt = $this->pdo->prepare($p_sSql);
        $stmt->execute();
        $arr = $stmt->fetchAll(\PDO::FETCH_CLASS, $p_sObject);
        if(!$arr)
            return $this->emptyReturnValue;
        $stmt = null;
        return $arr;
    }

    /**
     * Execute a query and return single key=>value array.
     * Provide $p_mCacheInterval to cache a queryresult
     * @param $p_sSql
     * @return array
     */
    function getResultKeyValue($p_sSql, $params = null){
        $stmt = $this->pdo->prepare($p_sSql);
        $stmt->execute($params);
        $arr = $stmt->fetchAll(\PDO::FETCH_KEY_PAIR);
        if(!$arr)
            return $this->emptyReturnValue;
        $stmt = null;
        return $arr;
    }

    /**
     * Execute query and return a single row as array
     * @param $p_sSql
     * @return array
     */
    function getRow($p_sSql, $params = null){
        $stmt = $this->pdo->prepare($p_sSql);
        $stmt->execute($params);
        $arr = $stmt->fetch(\PDO::FETCH_ASSOC);
        if(!$arr)
            return $this->emptyReturnValue;
        $stmt = null;
        return $arr;
    }

    /**
     * Execute query and return a single object
     * @param $p_sSql
     * @return ?object
     */
    function getObject($p_sSql, $p_sObject = "stdClass"){
        $stmt = $this->pdo->prepare($p_sSql);
        $stmt->execute();
        $obj = $stmt->fetchObject($p_sObject);
        if(!$obj)
            return null;
        return $obj;
    }

    /**
     * Insert $p_aData into table $p_sTable
     * @param $p_sTable
     * @return array)
     */
    function insertArray($p_sTable, $p_aData){

        $sValues = implode(",", array_fill(0, count($p_aData), "?"));
        $sCols = "";
        foreach($p_aData as $k => $v){
            $sCols .= "`{$k}`,";
        }
        $sCols = substr($sCols, 0, -1);

        $sSql = "INSERT INTO `{$p_sTable}` ($sCols) VALUES ({$sValues})";
        $stmt = $this->pdo->prepare($sSql);
        $bRes = $stmt->execute(array_values($p_aData));
        $aRes = array('success' => $bRes,
            'insert_id' => $this->pdo->lastInsertId());
        $stmt = null;

        return $aRes;
    }

    function insertAll($table, array $rows, array $options = []){
        if(count($rows) === 0)
            throw new \InvalidArgumentException('Must provide at least a single row.');
        $templateRow = $rows[0] ?? null;
        if(empty($templateRow['data']))
            throw new \InvalidArgumentException('First key in rows should be 0, use array_values to pass data');

        foreach($rows as $i => $row){
            if(!isset($row['data'])){
                throw new \InvalidArgumentException('A row must have a data key.');
            }
            $res = array_diff_key($templateRow['data'] ?? [], $row['data'] ?? []);
            $res2 = count($templateRow['data']) === count($row['data']);
            if(!empty($res) || !$res2){
                throw new \InvalidArgumentException("Row {$i} does not have the same keys as the first row");
            }
        }

        $columnList = implode(",", array_map(function ($s){
                return"`{$s}`";
            }, array_keys($templateRow['data'])));

        $rowPlaceHolder = "(" . implode(",", array_fill(1, count(array_keys($templateRow['data'])), "?")) . ")";

        $chunks = array_chunk($rows, ($options['chunksize'] ?? 100), true);
        foreach($chunks as $chunk){
            $sql = sprintf(
                'INSERT INTO %s (%s) VALUES %s',
                $table,
                $columnList,
                implode(', ', array_fill(1, count($chunk), $rowPlaceHolder))
            );
            $statement = $this->pdo->prepare($sql);
            $data = [];
            foreach($chunk as $row){
                $data = array_merge($data, array_values($row['data']));
            }
            $statement->execute($data);
        }
        return true;
    }

    /**
     * Update $p_aData in $p_sTable where $p_sColumn=$p_sValue
     * @param $p_sTable
     * @param $p_aData
     * @param $p_sColumn
     * @param $p_sValue
     * @return array query (insert-id)
     */
    function updateArray($p_sTable, $p_aData, $p_sColumn, $p_sValue){
        if(empty($p_sValue))
            throw new DbpException("Where-value cant be empty");

        $sCols = "";
        foreach($p_aData as $sKey => $sValue){
            $sCols .= " `{$sKey}`=?,";
        }
        $sCols = substr($sCols, 0, -1);

        $aParams = array_values($p_aData);
        $aParams[] = $p_sValue;

        $sSql = "UPDATE `{$p_sTable}` SET {$sCols} WHERE `{$p_sColumn}`=? ";
        $stmt = $this->pdo->prepare($sSql);
        $bRes = $stmt->execute($aParams);
        $aRes = array('success' => $bRes,
            'affected_rows' => $stmt->rowCount());
        $stmt = null;

        return $aRes;
    }

    /**
     * Return a single value from database
     * @param $p_sSql
     * @return string
     */
    function getValue($p_sSql, $params = null){
        $aRow = $this->getRow($p_sSql, $params);
        return is_array($aRow) ? array_shift($aRow) : null;
    }

    /**
     * Extract queries from given $p_sSrc and replace all placeolders with $p_aParams
     * Return the array with all queries to execute
     * @param $p_sSrc
     * @param $p_aParams
     * @return array
     * @throws DbpException
     */
    public function parseQueriesFromString($p_sSrc, $p_aParams = array()): array{

        $aQueries = array();
        $sSql = '';
        $iCounter = 0;
        $aRequiredParams = array();
        $aLines = explode("\n", $p_sSrc);

        # extract queries
        foreach($aLines as $sLine){
            if(preg_match_all('/\[([a-zA-Z0-9\_]+)\]/i', $sLine, $aMatches)){
                foreach($aMatches[1] as $sValue){
                    $aRequiredParams[$sValue] = true;
                }
            }
            if(preg_match("/^((?:[^\-]|\-(?!-))*)(?:\-\-.*)?\$/", $sLine, $aMatch)){
                $sSql = trim($sSql . " " . $aMatch[1]);
            }
            if(preg_match("/^(.*);\ *\$/", trim($sSql), $aMatch)){
                $aQueries[] = $aMatch[1];
                $iCounter++;
                $sSql = "";
            }
        }

        # check if required parameters are present
        $aMissing = array();
        foreach(array_keys($aRequiredParams) as $sKey){
            if(!isset($p_aParams[$sKey])){
                $aMissing[] = $sKey;
            }
        }
        if(!empty($aMissing)){
            throw new DbpException('Provide parameter for [' . implode("], [", $aMissing) . ']');
        }

        # replace variables
        foreach($aQueries as $iKey => &$sQuery){
            foreach($p_aParams as $sKey => $sValue){
                $sQuery = str_replace('[' . $sKey . ']', $sValue, $sQuery);
            }
        }

        return $aQueries;
    }

    /**
     * Read all queries in given p_sFilename, replace placeholders with $p_aParams
     * and execute queries. $p_bQuitOnError=true throws an DbpException - else it
     * will be handled and displayed in log
     * @param string $p_sFilename
     * @param array $p_aParams
     * @param bool $p_bQuitOnError
     * @return array
     * @throws DbpException
     */
    public function processFile($p_sFilename, $p_aParams = array(), $p_bQuitOnError = true): array{
        if(!file_exists($p_sFilename) || is_dir($p_sFilename) || !is_readable($p_sFilename)){
            throw new DbpException("IO error - cant read " . $p_sFilename);
        }

        $aLog = array();
        $sSrc = file_get_contents($p_sFilename);
        if(empty($sSrc))
            throw new DbpException("Source is empty");

        $aQueries = $this->parseQueriesFromString($sSrc, $p_aParams);

        $iCounter = 1;
        foreach($aQueries as $sSql){
            $aLog[] = "-- executing query {$iCounter}";
            $aLog[] = trim($sSql);
            $fStart = microtime(true);

            try{
                $iCounter++;
                $aRes = $this->query(trim($sSql));
                $sTime = number_format((microtime(true) - $fStart), 3, '.', '');
                $iNumRecords = isset($aRes['affected_rows']) ? " / " . $aRes['affected_rows'] . " affected" : null;
                $aLog[] = "-- OK {$sTime} sec / ok{$iNumRecords}";
            }
            catch(\PDOException $ex){
                $sTime = number_format((microtime(true) - $fStart), 3, '.', '');
                $aLog[] = "-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
                $aLog[] = "-- x ERROR {$sTime} sec | query {$iCounter} in " . basename($p_sFilename);
                $aLog[] = "-- x " . $ex->getMessage() . '';
                $aLog[] = "-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
                if($p_bQuitOnError === true){
                    throw new DbpException("-- exit processfile (\$p_bQuitOnError=true)");
                }
            }
            $aLog[] = "";
        }
        return $aLog;
    }

    /**
     * Alias for close()
     */
    public function disconnect(){
        $this->close();
    }

    /**
     * Close the connection
     */
    public function close(){
        $this->pdo = null;
        $this->connected = false;
    }

}

?>