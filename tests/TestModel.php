<?php

namespace kit;

class TestModel{

    private $id;
    private $label;
    private $date;

    function getId(){
        return $this->id;
    }

    function getLabel(){
        return $this->label;
    }

    function getDate(){
        return $this->date;
    }

    function setId($id){
        $this->id = $id;
    }

    function setLabel($label){
        $this->label = $label;
    }

    function setDate($date){
        $this->date = $date;
    }

}
