<?php

use PHPUnit\Framework\TestCase;

class dbpRepositoryTest extends TestCase{

    /**
     * @var \kit\dbp
     */
    var $oDb = null;

    /**
     * @var \kit\DbpRepository
     */
    var $joinedRepository;

    /**
     * @var \kit\DbpRepository
     */
    var $repository;

    function setUp():void{
        $this->setupMockedDatabase();

        // normal repository
        $this->repository = new kit\DbpRepository($this->oDb);
        $this->repository->init('test');

        // joined repository
        $this->joinedRepository = new kit\DbpRepository($this->oDb);
        $this->joinedRepository->init('test');
        $join = \kit\DbpJoinDefinition::fromArgs('lookup', 'l', 'id', 'id_lookup');
        $join->addField('label', 'lookup label');
        $join->addField('label', 'lookup label duplicate');
        $this->joinedRepository->joinTable($join);
    }

    function testGetId(){
        $res = $this->repository->getId(1);
        $this->assertArrayHasKey('id', $res);
        $this->assertEquals(1, $res['id']);
    }

    function testGetAll(){
        $res = $this->repository->getAll();
        $this->assertEquals(3, count($res));
        $this->assertEquals(1, $res[0]['id']);
    }

    function testSearch(){
        $res = $this->repository->search(['id', '=', 2]);
        $this->assertEquals(1, count($res));
        $this->assertEquals(2, $res[0]['id']);
    }

    function testSearchOrder(){
        $res = $this->repository->search(null, ['id', 'ASC'], 2);
        $this->assertEquals(2, count($res));
        $this->assertEquals(1, $res[0]['id']);


        $res = $this->repository->search(null, ['id', 'DESC'], 2);
        $this->assertEquals(2, count($res));
        $this->assertEquals(3, $res[0]['id']);
    }

    function testJoinedGetId(){
        $row = $this->joinedRepository->getId(3);
        $this->assertArrayHasKey('lookup label', $row);
        $this->assertArrayHasKey('lookup label duplicate', $row);
    }

    function testJoinedGetAll(){
        $res = $this->joinedRepository->getAll();
        $this->assertEquals(3, count($res));
        $this->assertArrayHasKey('lookup label', $res[0]);
    }

    function testJoinedSearch(){
        $res = $this->joinedRepository->search(['id', '=', 2]);
        $this->assertEquals(1, count($res));
        $this->assertEquals(2, $res[0]['id']);
        $this->assertArrayHasKey('lookup label', $res[0]);
    }

    function testJoinedSearchMulti(){
        $res = $this->joinedRepository->search([['id', '=', 2], ['id_lookup', '=', 2]]);
        $this->assertEquals(1, count($res));
        $this->assertEquals(2, $res[0]['id']);
        $this->assertArrayHasKey('lookup label', $res[0]);
    }

    function testJoinedSearchInjected(){
        $res = $this->joinedRepository->search(['id', '=', "asdf' AND (SELECT 7652 FROM (SELECT(SLEEP(5)))zASu)-- wbch"]);
        $this->assertNull($res);
    }

    function setupMockedDatabase(){

        $oPDO = new PDO(
            'sqlite::memory:', null, null, [
            \PDO::ATTR_EMULATE_PREPARES => false, // turn off emulation mode for "real" prepared statements
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION, //turn on errors in the form of exceptions
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC, //make the default fetch be an associative array
            ]
        );

        $this->oDb = kit\dbp::getInstance();
        $this->oDb->setPdoConnection($oPDO);

        $sCreateTestTable = "CREATE TABLE IF NOT EXISTS `test` (
          `id` int(11) NOT NULL ,
           `id_lookup` int(11) ,
          `label` varchar(255) NOT NULL DEFAULT '',
          PRIMARY KEY (`id`)
        ) ;
        ";
        $this->oDb->query($sCreateTestTable);
        $this->oDb->insertArray('test', ['id' => 1, 'label' => 'Test record 1', 'id_lookup' => 1]);
        $this->oDb->insertArray('test', ['id' => 2, 'label' => 'Test record 2', 'id_lookup' => 2]);
        $this->oDb->insertArray('test', ['id' => 3, 'label' => 'Test record 3']);

        $sCreateTestTable = "CREATE TABLE IF NOT EXISTS `lookup` (
          `id` int(11) NOT NULL ,
          `label` varchar(255) NOT NULL DEFAULT '',
          PRIMARY KEY (`id`)
        ) ;
        ";
        $this->oDb->query($sCreateTestTable);
        $this->oDb->insertArray('lookup', ['id' => 1, 'label' => 'Lookup record 1']);
        $this->oDb->insertArray('lookup', ['id' => 2, 'label' => 'Lookup record 2']);
    }

}
