<?php

use PHPUnit\Framework\TestCase;

class dbpTest extends TestCase{

    var $oDb = null;

    function setUp():void{

        $oPDO = new PDO(
            'sqlite::memory:', null, null, [
            \PDO::ATTR_EMULATE_PREPARES => false, // turn off emulation mode for "real" prepared statements
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION, //turn on errors in the form of exceptions
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC, //make the default fetch be an associative array
            ]
        );

        $this->oDb = kit\dbp::getInstance();
        $this->oDb->setPdoConnection($oPDO);
        $this->createTestTable();
    }

    function testInsert(){
        $aInsert = array('id' => mt_rand(123, 999999), 'label' => 'test', 'date' => date('Y-m-d'));

        $aRes = $this->insertTestRecord($aInsert);
        $this->assertTrue($aRes['success']);
        $this->assertGreaterThan(0, $aRes['insert_id']);
        // $this->assertEquals($aInsert['id'],$aRes['insert_id']); // TODO: caused by sqlite driver? $aRes['insert_id'] always 1

        $aDbRow = $this->oDb->getRow('SELECT * FROM test WHERE id=' . $aInsert['id']);
        $this->assertEquals($aInsert['label'], $aDbRow['label']);
        $this->assertEquals($aInsert['date'], $aDbRow['date']);
        $this->assertEquals($aInsert['id'], $aDbRow['id']);
    }

    function testUpdate(){

        $iID = mt_rand(1, 9999);
        $aRes = $this->insertTestRecord(array('id' => $iID, 'date' => date('Y-m-d')));

        $sNewLabel = "New label test " . mt_rand(1000, 9999);
        $aUpdateResult = $this->oDb->updateArray('test', array('label' => $sNewLabel), 'id', (int) $iID);

        $this->assertTrue($aUpdateResult['success']);
        $this->assertGreaterThan(0, $aUpdateResult['affected_rows']);

        $aDbRow = $this->oDb->getRow('SELECT * FROM test WHERE id=' . $iID);
        $this->assertEquals($sNewLabel, $aDbRow['label']);
    }

    function testGetObject(){

        $iID = mt_rand(100, 9999);
        $aRes = $this->insertTestRecord(array('id' => $iID, 'label' => 'test', 'date' => date('Y-m-d')));

        $oObj = $this->oDb->getObject("SELECT * FROM test WHERE id={$iID}", "kit\\TestModel");
        $this->assertEquals(get_class($oObj), 'kit\\TestModel');
        $this->assertEquals($oObj->getLabel(), "test");
    }

    function testObjectList(){

        $iID = mt_rand(100, 9999);
        $aRes = $this->insertTestRecord(array('id' => $iID, 'label' => 'test', 'date' => date('Y-m-d')));
        $iID = mt_rand(100, 9999);
        $aRes = $this->insertTestRecord(array('id' => $iID, 'label' => 'test', 'date' => date('Y-m-d')));
        $iID = mt_rand(100, 9999);
        $aRes = $this->insertTestRecord(array('id' => $iID, 'label' => 'test', 'date' => date('Y-m-d')));

        $arr = $this->oDb->getObjectList("SELECT * FROM test", "kit\\TestModel");
        $this->assertEquals(3, count($arr));
        $this->assertEquals("kit\\TestModel", get_class($arr[0]));
        $this->assertEquals("test", $arr[0]->getLabel());
    }

    function testInsertAll(){
        $data = $this->getInsertAllDataSetValid();
        $this->oDb->insertAll("test", $data);
        $this->assertEquals(10, $this->oDb->getValue("SELECT COUNT(*) FROM test"));
    }

    function testInsertAllChunked(){
        $data = $this->getInsertAllDataSetValid();
        $this->oDb->insertAll("test", $data, ['chunksize' => 5]);
        $this->assertEquals(10, $this->oDb->getValue("SELECT COUNT(*) FROM test"));
        $data = $this->getInsertAllDataSetValid();
        $this->oDb->insertAll("test", $data, ['chunksize' => 10]);
        $this->assertEquals(20, $this->oDb->getValue("SELECT COUNT(*) FROM test"));
    }

    function testInsertNoData(){
        $this->expectException(\InvalidArgumentException::class);
        $this->oDb->insertAll("test", []);
    }

    function testInsertAllInconsistentKeys(){
        $this->expectException(\InvalidArgumentException::class);
        $data = $this->getInsertAllDataSetValid();
        $data[3]['data']['test'] = "invalid entry";
        $this->oDb->insertAll("test", $data);
    }

    function testInsertAllDifferentKey(){
        $this->expectException(\InvalidArgumentException::class);
        $data = $this->getInsertAllDataSetValid();
        unset($data[3]['data']['id']);
        $data[3]['data']['test'] = "invalid entry";
        $this->oDb->insertAll("test", $data);
    }

    function testInsertAllDifferentKeyCaseSensitive(){
        $this->expectException(\InvalidArgumentException::class);
        $data = $this->getInsertAllDataSetValid();
        $testLbl = $data[3]['data']['label'];
        unset($data[3]['data']['label']);
        $data[3]['data']['Label'] = $testLbl;
        $this->oDb->insertAll("test", $data);
    }

    function testConnectionFailure(){
        $this->expectException(\PDOException::class);
        $db = new kit\dbp();
        $db->connect("localhost", "abc", "abc", "");
    }

    function createTestTable(){

        $sCreateTestTable = "CREATE TABLE IF NOT EXISTS `test` (
          `id` int(11) NOT NULL ,
          `label` varchar(255) NOT NULL DEFAULT '',
          `date` date NOT NULL,
          PRIMARY KEY (`id`)
        ) ;
        ";
        return $this->oDb->query($sCreateTestTable);
    }

    function insertTestRecord($p_aRecord = null): array{
        return $this->oDb->insertArray('test', $p_aRecord);
    }

    private function getInsertAllDataSetValid(){
        $data = array_map(
            function ($v){
                return ['data' => [
                    'id' => mt_rand(10000, 9999999),
                    'label' => "label " . mt_rand(1000, 9999),
                    'date' => date('Y-m-d')
                ]];
            },
            array_fill(0, 10, null));
        $data[5]['label'] = null;
        return $data;
    }

}
